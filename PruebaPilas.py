'''
Created on 8 oct. 2021

@author: arman
'''
class Pelicula:
    def __init__(self,nombre,genero):
        self.nombre=nombre
        self.genero=genero
    def __str__(self):
        return f"Nombre de la pelicula: {self.nombre}  Genero: {self.genero}"

class interfazRentaPelicula:
    def insetarPeliculaRentada(self,peli=Pelicula):
        pass
    def eliminarPelicula(self):
        pass
    def VerificarPilaVacia(self):
        pass
    def verificarPilaLlena(self):
        pass

class ImplementacionPilaEstatica(interfazRentaPelicula):
    def __init__(self,tamano):
        self.pilaPelis=[]
        self.limite=2
        self.cima=-1
    def VerificarPilaVacia(self):
        return self.cima==-1
    def verificarPilaLlena(self):
        return self.cima==self.limite
    def insetarPeliculaRentada(self, peli=Pelicula):
        if(self.verificarPilaLlena()==False):
            self.cima=self.cima+1;
            self.pilaPelis.append(peli)
            return True
        else:
            return False
    def eliminarPelicula(self):
        if(self.VerificarPilaVacia()==False):
            self.pilaPelis[self.cima]=""
            self.cima=self.cima-1
            return  True
        else:
            return False

    

#-------------------------------
opcionPeli=0
pelisDisponibles=0
listaPelis=[]
op=""
pilita=ImplementacionPilaEstatica(3)
while(op!="5"):
    print("Elige una opcion")
    print("1)Cargar BD peliculas")
    print("2)Rentar Pelicula")
    print("3)Devolver pelicula")
    print("4)Mostrar cantidad de peliculas disponibles para la renta")
    print("5)Salir")
    op=input()
    if(op=="1"):
        listaPelis.append(Pelicula("Las cronicas de Spiderwick ","Fantasia"))
        listaPelis.append(Pelicula("Harry Potter","Fantasia"))
        listaPelis.append(Pelicula("Terminator","Ciencia Ficcion"))
        listaPelis.append(Pelicula("Venom","Ciencia ficcion"))
        listaPelis.append(Pelicula("Reminiscencia","Ciencia ficcion"))
        print("BD de peliculas cargada")
    elif(op=="2"):
        op2=0
        i=0
        if(len(listaPelis)>0):
            print("Que pelicula deseas rentar: ")
            for i in range(i,len(listaPelis)):
                print(f"{(i+1)}) {listaPelis[i]}")
                i=i+1
            try:
                op2=int(input())
                if(op2<=len(listaPelis)):
                    if(pilita.insetarPeliculaRentada(listaPelis[op2-1])):
                        print("La pelicula se a rentado")
                        pelisDisponibles=pelisDisponibles+1
                    else:
                        print("No se a podidio agregar la pelicula")
                else:
                    print("No ingresaste una opcion disponible")
            except ValueError:
                print("No ingresaste un numero")
        else:
            print("Vaya alparecer no has cargado peliculas")
    elif(op=="3"):
        if( pilita.eliminarPelicula()):
            print("Se a devvuelto la pelicula")
            pelisDisponibles=pelisDisponibles-1
        else:
            print("No se a devuelto la pelicula al parecer no tienes peliculas rentadas")
    elif(op=="4"):
        if(pelisDisponibles!=5):
            print(f"Puedes rentar aun {(5-pelisDisponibles)}")
        else:
            print("Oh vaya ya no puedes rentar mas peliculas")